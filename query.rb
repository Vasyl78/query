class Query
  require 'json'

  class << self
    def filter(&block)
    end

    def from(*args)
      new.from(*args)
    end

    def size(*args)
      new.size(*args)
    end

    def sort(*args)
      new.sort(*args)
    end
  end

  def query
    @query ||= {query: {}}
  end

  def filter(&block)
  end

  def from(from)
    query[:from] = from
    self
  end

  def size(size)
    query[:size] = size
    self
  end

  def sort(*args)
    query[:sort] = {}
    args.each do |arg|
      case arg
        when Symbol
          query[:sort][arg] = :asc
        when Hash
          arg.each do |key, value|
            next unless %i(asc desc).include?(value)
            query[:sort][key] = value
          end
      end
    end
    self
  end

  def to_json
    query.to_json    
  end
end
